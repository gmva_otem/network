# Auteurs du projet - Partie Réseau API/BDD

## Scrum Master
- Clément Maillet

## Contributeurs
- Clément Maillet (https://gitlab.com/Karbbone56)
- Benjamin Le Bouëdec (https://gitlab.com/Benjamin_LB)
- Sophie Bertrand (https://gitlab.com/AlphardHydrae)
- Mattéo Le Poupon (https://gitlab.com/Aenigmant)

## Développeurs

- Sophie Bertrand
    - Création du docker, mise en place d'un serveur MQTT Mosquitto et d'un serveur Ubuntu pour le Docker  
    - Conversion des données en format JSON vers l'API et mise en place d'un système de données temporaires
- Benjamin Le Bouëdec  
    - Insertion des données avec influx_handler, conversion des données en format JSON vers l'API et mise en place d'un système de données temporaires
    - Création de la carte, mise en place des capteurs et passerelles + création du tableau des données
- Mattéo Le Poupon 
    - Insertion des données avec influx_handler, conversion des données en format JSON vers l'API
- Clément Maillet
    - Mise en place d'un système de données temporaires,
    - Front de l'application web + back avec ajout du dark mode / light mode

## Documentation
- Benjamin Le Bouëdec 
    - Compte rendu d'installation de InfluxDB et de création de la base de données
- Sophie Bertrand 
    - Compte rendu d'installation d'un serveur MQTT avec Mosquitto
- Mattéo Le Poupon 
    - Compte rendu d'installation de l'API
- Clément Maillet 
    - Compte rendu d'installation de InfluxDB et compte rendu de la syntaxe des messages en JSON  
      
## Conception

- Clément Maillet
    - Diagramme de cas d'utilisation et diagramme de séquence de l'affichage des données avec fetchAll et fetchLast
- Mattéo Le Poupon
    - Diagramme de séquence de la connexion MQTT et l'insertion des données, ainsi que le diagramme de Gant
- Benjamin Le Bouëdec
    - Diagramme de classes
