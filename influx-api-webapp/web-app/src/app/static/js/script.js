import { url, pwd } from './config.js'; // import de l'url et du mot de passe depuis le fichier config

let sensor1; // capteur 1
let sensor2; // capteur 2
let doc;  // variable qui stocke le résultat de la méthode GET sur l'API
let cptNav = 0; // compteur nav

var urlLast = url + "?password=" + pwd; // url pour récupérer les données


/** Sauvegarde des données de l'API dans la variable doc
 * 
 */
$(document).ready(function() {
  $.getJSON(urlLast, function(json) {
    doc = json;
  });

  /**
   * ajoute le fait de cliqué sur le header pour changer de page ici on ajoute le texte du tuto et le h1
   */
  $("#tH").click(function () {
    console.log("2")
    $("#map").hide();
    $("#tuto").show();
    if (cptNav < 1) {
      $("#tuto").append("<h1>Tutoriel</h1>");
      $("h1").css("font-family", "Caviar")
      $("#tuto").css("text-align", "center");
      $("#tuto").append("<p class=texteTuto>Cette application web sert à visualiser les données des capteurs via la map leaflet disponible sur la page carte. Pour accéder aux données d'un capteur il suffit de cliquer sur une passerelle qui lui est associée </p>")
      $("#tuto").append("<p class=texteTuto>Une fois cliqué le capteur va s'afficher sur la map et une fois le curseur sur le capteur il permet d'afficher dans une bulle les données récupérées par ce capteur </p>")
      $("p").css("margin","30px")
      $("p").css("font-family", "Caviar")
      $(".texteTuto").css('font-size', "22px");

    }
    cptNav++;
  });
  /**
   * lorsequ'on veut passer de tuto a map
   */
  $("#cH").click(function () {
    console.log("1")
    $("#map").show();
    $("#tuto").hide();
  });
});


let mymap = L.map("map").setView([47.581852, -2.752247], 13); // création de la carte


var legend = L.control({position: 'topright'}); // initialisation de la légende



/** Configuration de la légende
 * 
 */
legend.onAdd = function (mymap) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = ["Passerelle 1", "Capteur 1", "Passerelle 2", "Capteur 2"],
        labels = ['static/img/gateway1.png', 'static/img/sensor1.png', 'static/img/gateway2.png', 'static/img/sensor2.png'];

    $(div).addClass("lh-base bg-light mb-3 border border-dark border-3 border-opacity-50");

    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
          '<img src="' + labels[i] + '" style="width:2vw;height:2vw;">' +
          '   ' + grades[i] + '<br>';
    }

    return div;
};

legend.addTo(mymap); // ajout de la légende sur la carte

var legendDiv = $('.info.legend');  // récupération de la div pour la légende


// Affichage de la carte
L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution:
    '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
}).addTo(mymap); 


// Création de l'icône de la première passerelle
var gateway1Icon = L.icon({
  iconUrl : 'static/img/gateway1.png',
  iconSize: [30,35]
})

// Création de l'icône de la deuxième passerelle
var gateway2Icon = L.icon({
  iconUrl : 'static/img/gateway2.png',
  iconSize: [30,35]
})

// Création de l'icône du premier capteur
var sensor1Icon = L.icon({
  iconUrl : 'static/img/sensor1.png',
  iconSize: [25,25]
})

// Création de l'icône du deuxième capteur
var sensor2Icon = L.icon({
  iconUrl : 'static/img/sensor2.png',
  iconSize: [25,25]
})


// Création de la première passerelle 
var gateway1 = L.marker([47.589841, -2.752075], {icon : gateway1Icon}).on("click", gateway1Click).addTo(mymap);

gateway1.bindPopup("<p>Passerelle °1</p>"); // Ajout d'une bulle sur la première passerelle
gateway1.on("mouseover", function (e) {     // ouverture de la bulle lorsque l'utilisateur glisse la souris sur la passerelle
  gateway1.openPopup();
});
gateway1.on("mouseout", function (e) {      // fermeture de la bulle lorsque l'utilisateur n'a plus le pointeur de souris sur la passerelle
  gateway1.closePopup();                   
});

// Création de la deuxième passerelle 
var gateway2 = L.marker([47.584805, -2.761259], {icon : gateway2Icon}).on("click", gateway2Click).addTo(mymap);


gateway2.bindPopup("<p>Passerelle °2</p>");  // Ajout d'une bulle sur la deuxième passerelle
gateway2.on("mouseover", function (e) {      // ouverture de la bulle lorsque l'utilisateur glisse la souris sur la passerelle
  gateway2.openPopup();
});
gateway2.on("mouseout", function (e) {       // fermeture de la bulle lorsque l'utilisateur n'a plus le pointeur de souris sur la passerelle
  gateway2.closePopup();
});

// Création du capteur associé à la première passerelle
sensor1 = L.marker([47.590435, -2.753835], {icon : sensor1Icon});

// Création du capteur associé à la deuxième passerelle
sensor2 = L.marker([47.585847, -2.762289], {icon : sensor2Icon});

/**Affichage des données de la première passerelle et le capteur associé 
 * 
*/
function gateway1Click() {

  var temp1 = doc.data[0].temp; // récupération de la température récoltée par le premier capteur

  var date1 = new Date(parseInt(doc.data[0].date));  // récupération de la date
  var jour1 = date1.getDate(); // récupération du jour 
  var mois1 = date1.getMonth() + 1; // récupération du mois 
  var annee1 = date1.getFullYear(); // récupération de l'année

  var time1 = new Date(parseInt(doc.data[0].time)); // récupération du temps
  var heure1 = time1.getHours();  // récupération de l'heure
  var minutes1 = time1.getMinutes(); // récupération de la date
  var secondes1 = time1.getSeconds(); // récupération des secondes

  if(secondes1 < 10){
    secondes1 = "0" + secondes1;  // ajout d'un 0 en préfixe si les secondes sont inférieures à 10
  }

  // format d'affichage de la date
  var dateFormat1 = jour1 + "/" + mois1 + "/" + annee1 + " " + heure1 + ":" + minutes1 + ":" + secondes1 + " ";

  sensor1.addTo(mymap); // ajout du premier capteur sur la carte
  sensor1.bindPopup(`<p>Température : ${temp1}°C</p><p>Date du dernier relevé : ${dateFormat1}</p><button class="btn btn-primary btn-sm
  ">En savoir plus</button>`);  // affichage des données dans la bulle
  sensor1.on("mouseover", function (e) {
    sensor1.openPopup();  // ouverture de la bulle lorsque l'on passe la souris dessus
  });
}

/**Affichage des données de la deuxième passerelle et le capteur associé 
 * 
*/
function gateway2Click() {

  var temp2 = doc.data[1].temp; // récupération de la température récoltée par le deuxième capteur

  var date2 = new Date(parseInt(doc.data[1].date)); 
  var jour2 = date2.getDate();
  var mois2 = date2.getMonth() + 1;
  var annee2 = date2.getFullYear();

  var time2 = new Date(parseInt(doc.data[1].time));
  var heure2 = time2.getHours();
  var minutes2 = time2.getMinutes();
  var secondes2 = time2.getSeconds() < 10 ? '0':'';

  if(secondes2 < 10){
    secondes2 = "0" + secondes2;
  }

  var dateFormat2 = jour2 + "/" + mois2 + "/" + annee2 + " " + heure2 + ":" + minutes2 + ":" + secondes2 + " ";

  sensor2.addTo(mymap);
  sensor2.bindPopup(`<p>Température : ${temp2}°C</p><p>Date du dernier relevé : ${dateFormat2}</p><button class="btn btn-primary btn-sm
  ">En savoir plus</button>`);
  sensor2.on("mouseover", function (e) {
    sensor2.openPopup();
  });
}


var header = $('#navH');  // header de l'application
var body = $('body');     // corps de l'application
var a = $('p');           // texte du footer
var foot = $('#footer');  // footer de l'application


/** Passage du thème clair au thème sombre
 * 
 */
$('#flexSwitchCheckReverse').change(function () {
  if ($(this).prop('checked')) {
    header.removeClass('bg-white'); // suppression du thème clair pour l'arrière plan du header
    header.addClass('navbar-dark'); // ajout du thème sombre pour la barre de navigation
    body.addClass('bg-dark'); // ajout du thème sombre pour l'arrière plan du corps de la page
    body.removeClass('bg-white'); // suppression du thème clair pour l'arrière plan du corps de la page
    legendDiv.removeClass('bg-light'); // suppression du thème clair pour la légende de la carte
    legendDiv.addClass('bg-dark'); // ajout du thème sombre pour la légende de la carte
    legendDiv.addClass('text-light'); // ajout du thème clair pour le texte de la légende
    legendDiv.removeClass('border-dark'); // suppression du thème sombre pour la bordure de la légende
    legendDiv.addClass('border-light'); // ajout du thème clair pour la bordure de la légende
    foot.addClass('bg-dark');  // ajout du thème sombre pour l'arrière plan du footer
    a.addClass('text-light');  // ajout du thème clair pour le texte du footer
    $('.moon').attr('src', 'static/img/dark_moon.png'); // paramétrage de l'icône moon en mode sombre
    $('.sun').attr('src', 'static/img/dark_sun.png'); // paramétrage de l'icône sun en mode sombre
    $('.logo').attr('src', 'static/img/logo_dark.png'); // paramétrage du logo en mode sombre
    var layer = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: 'Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.'
    }).addTo(mymap);  // affichage de la carte Leaflet en mode sombre
    $("#tuto").removeClass("bg-white text-dark"); // supression du tutoriel en clair
    $("#tuto").addClass("bg-dark text-light"); // affichage du tutoriel en sombre
  } else {
    header.removeClass('navbar-dark'); // suppression du thème sombre pour la barre de navigation
    header.addClass('bg-white');  // ajout du thème clair pour l'arrière plan du header
    body.addClass('bg-white'); // ajout du thème clair pour l'arrière plan du corps de la page
    body.removeClass('bg-dark') // suppression du thème sombre pour l'arrière plan du corps de la page
    legendDiv.removeClass('bg-dark'); // suppression du thème sombre pour la légende de la carte
    legendDiv.addClass('bg-light'); // ajout du thème clair pour la légende de la carte
    legendDiv.removeClass('text-light'); // suppression du thème clair pour le texte de la légende
    legendDiv.removeClass('border-light'); // suppression du thème clair pour la bordure de la légende
    legendDiv.addClass('border-dark'); // ajout du thème sombre pour la bordure de la légende
    foot.removeClass('bg-dark'); // suppression du thème sombre pour l'arrière plan du footer
    a.removeClass('text-light'); // suppression du thème clair pour le texte du footer
    $('.moon').attr('src', 'static/img/light_moon.png'); // paramétrage de l'icône moon en mode clair
    $('.sun').attr('src', 'static/img/light_sun.png'); // paramétrage de l'icône sun en mode clair
    $('.logo').attr('src', 'static/img/logo.jpg'); // paramétrage du logo en mode clair
    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      maxZoom: 19,
      attribution:
        '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    }).addTo(mymap);  // affichage de la carte Leaflet en mode clair
    $("#tuto").removeClass("bg-dark text-light"); // supression du tutoriel en sombre
    $("#tuto").addClass("bg-white text-dark"); // affichage du tutoriel en clair
  }
});