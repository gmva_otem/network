# Mise en place d'une interface web de visualisation de données

## Structure

```
static
└── css
    └── style.css
└── fonts
    └── # fonts
└── img
    └── # images
└── js
    ├── # config.js
    └── script.js
└── lib
    └── # libs
index.html
docker-compose.yml
README.md
```

## Installation

Pour installer l'application

```bash
git clone https://gitlab.com/gmva_otem/network.git
```

## Configuration

### Création du fichier _config_

Créer le fichier config.js

static/js/config.js

```js
export const url = "url";
export const pwd = "pwd";
```

## Accéder à l'application

Lancer le client

Éventuellement

```bash
docker-compose build
```

puis

```bash
docker-compose up
```

Accéder à

```
http://localhost:4000/
```
