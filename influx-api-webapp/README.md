Le projet a pour objectif de suivre et de valoriser des données de
suivi des températures d'eau de mer du Golfe du Morbihan. Ce suivi
pourra ainsi être intégré à l'Observatoire des effets du changement
climatique porté par Golfe du Morbihan Vannes Agglomération (GMVA).

Dans la partie réseau de ce projet, il s'agit de mettre en œuvre
l'infrastructure réseau de ce projet.

Pour ce faire, il faut

- choisir le matériel pour concevoir une passerelle LoRa/LoRaWAN;
- installer et configurer cette passerelle pour qu'elle puisse
  recevoir les données des capteurs et les retransmettre vers le
  réseau [TheThingsNetwork](https://www.thethingsnetwork.org/);
- rendre accessible les données collectées à l'application Web de
  visualisation des données depuis TheThingsNetwork;
- tester en conditions réelles cette passerelle.

## Installation et utilisation

Créer le fichier _.env_

.env

```
INFLUXDB_TOKEN="token"
INFLUXDB_USERNAME="user"
INFLUXDB_PASSWORD="pwd"
INFLUXDB_ORG="org"
INFLUXDB_BUCKET="bucket"
```

Éventuellement

```bash
docker-compose build
```

puis

```bash
docker-compose up
```