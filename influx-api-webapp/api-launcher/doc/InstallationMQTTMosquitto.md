# Installation client MQTT Mosquitto

L'installation de ce client permet de distribuer les paquets de données depuis l'hébergeur entre les différentes machines de l'utilisateur.


## Documentation

- [Repo Git Mosquitto](https://github.com/eclipse/mosquitto)
- [Installation Mosquitto](https://mosquitto.org/download/)
- [Tests Mosquitto](https://test.mosquitto.org)


## Installation

#### MacOS
```bash
brew install mosquitto
```

#### Linux
```bash
snap install mosquitto
```

#### Ubuntu
```bash
sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
sudo apt-get update
```
## Lancement du client

Pour lancer le client

```bash
mosquitto -c /path_to_config_file
```

Le fichier mosquitto.conf doit contenir les informations suivantes

```bash
cat /path_to_config_file
```
```
listener 1883
protocol mqtt
allow_anonymous true
```

Pour recevoir les informations envoyées à l'application

```bash
mosquitto_sub -h eu1.cloud.thethings.network -t +/devices/+/up -u user_id -P api_key -d
```

Pour envoyer des informations à l'application

```bash
mosquitto_pub -h eu1.cloud.thethings.network -t "v3/app_name/devices/device_name/down/push" -u user_id -P api_key -m 'json_data' -d
```
