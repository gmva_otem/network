# Compte rendu d'installation de InfluxDB

InfluxDB est le système de gestion de base de données (SGBD) qui va être utilisé pour la création de la base de données.  


## Installation d'InfluxDB

Installer InfluxDB :   
```
wget https://dl.influxdata.com/influxdb/releases/influxdb2-2.5.1-amd64.deb
```
```
sudo dpkg -i influxdb2-2.5.1-amd64.deb
```


## Installation du CLI d'InfluxDB

Télécharger le package :
```
wget https://dl.influxdata.com/influxdb/releases/influxdb2-client-2.5.0-linux-amd64.tar.gz
```

Décompresser le package :
```
tar xvzf path/to/influxdb2-client-2.5.0-linux-amd64.tar.gz
```

Ajouter l'exécutable influx dans le PATH :
```
sudo cp influxdb2-client-2.5.0-linux-amd64/influx /usr/local/bin/
```


## Mise en service d'InfluxDB

Démarrer le service InfluxDB :
```
sudo service influxdb start
```
Arrêter le service InfluxDB :
```
sudo service influxdb stop
```
Redémarrer le service InfluxDB :
```
sudo service influxdb restart
```


## Lancement du shell InfluxDB version 1.x

Démarrer le shell influx v1 :  
```
influx v1 shell
```

Quitter le shell influx v1 :
```
quit
```

Une fois connecté au shell v1, la création de la base de données va pouvoir démarrer.


## (Optionnel) Désinstallation d'InfluxDB

Désinstaller InfluxDB :

```
sudo apt-get purge --auto-remove influxdb  
```


