
# Format du fichier .JSON

Voici le format du fichier JSON

```json
{
  "idSensor" : 1,
  "idFloater" : 1,
  "date" : "23/11/2022",
  "time" : "18:00",
  "temp" : 18
}
```

### Correspondant à ces tables

**SENSOR**

| idSensor (1) : int | depth : int| @ theFloater(idFloater) : int(1)
| :-------- | :------- | :------- |

**DATA**

| idData (1) : int | @ theFloater(idFloater) : int | date : Date | time : Time | temp : double | @ theSensor = Sensor(idSensor) : int
| :-------- | :------- | :------- | :------- | :------- | :------- |

**FLOATER**

| idFloater (1) : int | coordX : double | coordY: double | desc : String
| :-------- | :------- | :------- | :-------|

**GATEWAY**

| idGateway (1) : int | coordX : double | coordY: double | desc : String
| :-------- | :------- | :------- | :------- |

**LINK**

| @ theFloater = Floater(idFloater) (1) : int| @ theGateway = Gateway(idGateway) (1) : int|
| :-------- | :------- |

