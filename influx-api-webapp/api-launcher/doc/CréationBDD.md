# Création et configuration de la base de données

Créer une base de données :  
```
influx bucket create -n nom_database
```

**Un exemple de création d'une base de données :**
```
influx bucket create -n otem
```

Supprimer une base de données :
```
influx bucket delete -n nom_database
```
    
Visualiser la liste des bases de données créées :  
```SQL
show databases
```

Se connecter à la base de données :
```SQL
use nom_database
```

Ajouter des données dans une base de données :
```SQL
insert nomTable,attIndicé=val att1=valeur1,att2=valeur2,att3=valeur3
```
    
Cette commande est divisée en plusieurs parties. Tout d'abord, on spécifie le nom de la table à ajouter dans la base de données.
Ensuite, un ou plusieurs 'tags' (le ou les attributs indicés qui forment la clé primaire) peuvent être précisés. 

Cependant, sur InfluxDB, l'attribut principal de la clé primaire sera toujours 'time' : il n'est donc pas nécessaire d'ajouter des tags dans une table.   

Enfin, la dernière partie regroupe l'ensemble des attributs de la table rattachés à une valeur (qui doit être renseignée lors de l'insertion).  

**Un exemple d'ajout de données dans une table nommée data avec comme attributs idSensor et temp :**
```SQL
insert data,idData=1 theFloater=1,date="07/12/2022",time="16:13:00",temp=-12,theSensor=1
```

Visualiser la liste des tables : 
```SQL
show measurements
```

Supprimer une table : 
```SQL
drop measurement nomTable
```

Supprimer des données d'une table :
```SQL
delete from nomTable where time < 'YY-MM-JJ HH:MM:SS'
```
**Un exemple de suppression de données dans la table data :**
```SQL
delete from data where time < '2022-11-16 19:45:01'
```


