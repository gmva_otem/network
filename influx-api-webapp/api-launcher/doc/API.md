# Utilisation de l'API REST 
Cette api a pour but de renvoyer les données récupérer par les passerelles pour les renvoyer sous format json. Pour récupérer ces données vous devrez réaliser les étapes suivantes :
## Prérequis
Créer un compte influxDB
```
http://localhost:4096
```
# A partir de l'UI d'influxDB

Créer un TOKEN pour l'api. Pour ce faire il faut aller dans API TOKEN et faire create new.<br>
Une fois créé, garder ce token.

# A partir du shell
```
cd network/src
docker exec -it influx-container shell
```
Pour configurer influxDB
```
influx setup
```
Entrer les informations nécessaires puis copier le token api sur
```
influx auth list
```
# Configuration du fichier config.js

scripts/config.js
```
const influx_host = "http://influx-container:8086";
const influx_token = "token";
const influx_org = "org";
const influx_db = "bucket";

module.exports = {
  influx_host: influx_host,
  influx_token: influx_token,
  influx_org: influx_org,
  influx_db: influx_db,
};
```

## Accès à l'API REST OTEM
Lien vers l'API : http://localhost:8192

## Lien vers les routes
Accéder à toutes les données
```
 http://localhost:8192/fetchAll
```
Accéder à la dernière donnée
```
http://localhost:8192/fetchLast
```

# Lancer l'api
Depuis le fichier src
```
docker-compose build && docker-compose up
```

