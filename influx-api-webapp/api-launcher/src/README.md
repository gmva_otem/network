# Mise en place d'un script de récupération et d'insertion de données depuis un serveur distant vers une base de données et d'une API Express

## Structure

```
api
└── controllers
    └── controllers.js
└── routes
    └── routes.js
└── services
    ├── API.js
    ├── # config.js
    └── password_handler
├── app.js
├── Dockerfile
├── package.json
├── # server.csr
└── # server.key
script
├── app.js
├── # config.js
├── Dockerfile
├── influx_handler.js
├── mqtt_handler.js
└── package.json
docker-compose.yml
# .env
README.md
```

## Installation

Pour installer le client

```bash
git clone https://gitlab.com/gmva_otem/network.git
```

## Utilisation du script de récupération et d'insertion des données

Pour lancer le client

```bash
cd network/api-launcher/src
docker-compose build && docker-compose up
```

#### Configurer InfluxDB via le shell

Au besoin, il est possible d'accéder au shell du container influxdb

```bash
cd network/api-launcher/src
docker exec -it influx-container shell
```

ou bien à son interface graphique à partir de

```
http://localhost:4096/
```

### Création des fichiers _config_

Créer les fichiers config.js

api/services/config.js

```js
const influx_host = "http://influx-container:8086";
const influx_token = "token";
const influx_user = "user";
const influx_password = "pwd";
const influx_org = "org";
const influx_db = "bucket";

const userpwd = "apiPassword";

module.exports = {
  influx_host: influx_host,
  influx_token: influx_token,
  influx_user: influx_user,
  influx_password: influx_password,
  influx_org: influx_org,
  influx_db: influx_db,
  userpwd: userpwd,
};
```

scripts/config.js

```js
const mqtt_host = "mqtt://eu1.cloud.thethings.network";
const mqtt_username = "username@ttn";
const mqtt_password = "token";

const influx_host = "http://influx-container:8086";
const influx_token = "token";
const influx_user = "user";
const influx_password = "pwd";
const influx_org = "org";
const influx_db = "bucket";

module.exports = {
  mqtt_host: mqtt_host,
  mqtt_username: mqtt_username,
  mqtt_password: mqtt_password,
  influx_host: influx_host,
  influx_token: influx_token,
  influx_user: influx_user,
  influx_password: influx_password,
  influx_org: influx_org,
  influx_db: influx_db,
};
```

Créer le fichier _.env_

.env

```
INFLUXDB_TOKEN="token"
INFLUXDB_USERNAME="user"
INFLUXDB_PASSWORD="pwd"
INFLUXDB_ORG="org"
INFLUXDB_BUCKET="bucket"
```

## Accéder à l'API

Lancer le client:

Éventuellement

```bash
docker-compose build
```

puis

```bash
docker-compose up
```

Accéder à

```
http://localhost:8192/${route}?password=${password}
```

#### Les routes mises à disposition sont les suivantes

- /fetchAll # qui renvoie l'intégralité des données et purge le bucket
- /fetchAllKeep # qui renvoie l'intégralité des données sans purger le bucket
- /fetchLast # qui renvoie la dernière donnée relevée
- /fetchGroup # qui renvoie l'id et la position des capteurs et des passerelles qui leurs sont associées
- /fetchBy/${id} # qui renvoie la dernière donnée relevée par le capteur spécifié
