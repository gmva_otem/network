/**
 * This is the handler of the queries to the database
 */

const { InfluxDB } = require("@influxdata/influxdb-client");
const { DeleteAPI } = require("@influxdata/influxdb-client-apis");
const host = require("./config").influx_host;
const token = require("./config").influx_token;
const org = require("./config").influx_org;
const db = require("./config").influx_db;

class API {
  constructor() {
    this.host = host;
    this.token = token;
    this.org = org;
    this.db = db;

    // establishing a connection with the database
    this.influxClient = new InfluxDB({
      url: this.host,
      token: this.token,
    });

    // creating a query API
    this.queryApi = this.influxClient.getQueryApi(this.org);

    // creating a delete API
    this.deleteApi = new DeleteAPI(this.influxClient);
  }

  // fetches all the data from the database without purging the database
  dataFetchAllKeep() {
    // query to fetch all the data from the database
    let query = `from(bucket: "${this.db}")
                |> range(start: 0)
                |> filter(fn: (r) => r["_measurement"] == "data")
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")`;

    return new Promise((resolve, reject) => {
      let ret = [];

      // for each row of the query, it pushes the data in the ret array following a set format
      this.queryApi.queryRows(query, {
        next(row, tableMeta) {
          let o = tableMeta.toObject(row);

          ret.push({
            id: o._time,
            idFloater: o.idFloater,
            idSensor: o.idSensor,
            lat: o.latitude,
            long: o.longitude,
            date: o.measurementDate,
            time: o.measurementTime,
            temp: o.measurementTemp,
          });
        },
        error(error) {
          console.error(error);
          console.log("fetch all keep failed");
          reject(error);
        },
        complete() {
          console.log("fetch all keep success");
          resolve(ret);
        },
      });
    });
  }

  // fetches all the data from the database
  dataFetchAll() {
    // query to fetch all the data from the database
    let query = `from(bucket: "${this.db}")
                |> range(start: 0)
                |> filter(fn: (r) => r["_measurement"] == "data")
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")`;

    return new Promise((resolve, reject) => {
      let ret = [];

      // for each row of the query, it pushes the data in the ret array following a set format
      this.queryApi.queryRows(query, {
        next(row, tableMeta) {
          let o = tableMeta.toObject(row);

          ret.push({
            id: o._time,
            idFloater: o.idFloater,
            idSensor: o.idSensor,
            lat: o.latitude,
            long: o.longitude,
            date: o.measurementDate,
            time: o.measurementTime,
            temp: o.measurementTemp,
          });
        },
        error(error) {
          console.error(error);
          console.log("fetch all failed");
          reject(error);
        },
        complete() {
          console.log("fetch all success");

          this.deleteApi.postDelete({
            org: this.org,
            bucket: this.db,
            body: {
              start: "2020-01-01T00:00:00.00Z",
              stop: new Date(),
              predicate: '_measurement="data"',
            },
          });

          resolve(ret);
        },
      });
    });
  }

  // fetches the last entry of the database
  dataFetchLast() {
    // query to fetch the last entry of the database
    let query = `from(bucket: "${this.db}")
                |> range(start: 0)
                |> filter(fn: (r) => r["_measurement"] == "data")
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                |> last(column: "_time")`;

    return new Promise((resolve, reject) => {
      let ret = [];

      // pushes the data in the ret array following a set format
      this.queryApi.queryRows(query, {
        next(row, tableMeta) {
          let o = tableMeta.toObject(row);

          ret.push({
            id: o._time,
            idFloater: o.idFloater,
            idSensor: o.idSensor,
            lat: o.latitude,
            long: o.longitude,
            date: o.measurementDate,
            time: o.measurementTime,
            temp: o.measurementTemp,
          });
        },
        error(error) {
          console.error(error);
          console.log("fetch last failed");
          reject(error);
        },
        complete() {
          console.log("fetch last success");
          resolve(ret);
        },
      });
    });
  }

  // fetches the data from the database grouped by idSensor
  dataFetchGroup() {
    // query to fetch the data from the database grouped by idSensor
    let query = `from(bucket: "${this.db}")
                |> range(start: 0)
                |> filter(fn: (r) => r["_measurement"] == "data")
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                |> group(columns: ["idSensor"], mode: "by")
                |> unique(column: "idSensor")`;

    return new Promise((resolve, reject) => {
      let ret = [];

      // pushes the data in the ret array following a set format
      this.queryApi.queryRows(query, {
        next(row, tableMeta) {
          let o = tableMeta.toObject(row);

          ret.push({
            idFloater: o.idFloater,
            idSensor: o.idSensor,
            lat: o.latitude,
            long: o.longitude,
          });
        },
        error(error) {
          console.error(error);
          console.log("fetch group failed");
          reject(error);
        },
        complete() {
          console.log("fetch group success");
          resolve(ret);
        },
      });
    });
  }

  // fetches the data from the database from a specific idSensor
  dataFetchBy(sensor) {
    // query to fetch the data from the database from a specific idSensor
    let query = `from(bucket: "${this.db}")
                |> range(start: 0)
                |> filter(fn: (r) => r["_measurement"] == "data")
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                |> filter(fn: (r) => r["idSensor"] == "${sensor}")
                |> last(column: "_time")`;

    return new Promise((resolve, reject) => {
      let ret = [];

      // pushes the data in the ret array following a set format
      this.queryApi.queryRows(query, {
        next(row, tableMeta) {
          let o = tableMeta.toObject(row);

          ret.push({
            idFloater: o.idFloater,
            idSensor: o.idSensor,
            temp: o.measurementTemp,
          });
        },
        error(error) {
          console.error(error);
          console.log("fetch by failed");
          reject(error);
        },
        complete() {
          console.log("fetch by success");
          resolve(ret);
        },
      });
    });
  }
}

module.exports = new API();
