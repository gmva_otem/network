/**
 * This is the handler of the api connection
 */

const crypto = require("crypto-js");
const userpwd = require("./config").userpwd;

class PasswordHandler {
  constructor(password) {
    this.password = password;
    this.userpassword = userpwd;
  }

  // hash the password
  hashPassword(pwd) {
    return crypto.SHA256(pwd).toString(crypto.enc.Hex);
  }

  // compare the password with the stored hash
  comparePasswords() {
    return this.hashPassword(this.password) === this.userpassword;
  }
}

module.exports = PasswordHandler;
