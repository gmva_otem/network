/**
 * This handles the routes of the API
 */

const express = require("express");
const router = express.Router();
const controller = require("../controllers/controllers");

router.get("/fetchAll", controller.getAll); // route to fetch all entries from the database
router.get("/fetchAllKeep", controller.getAllKeep); // route to fetch all entries from the database without purging the database
router.get("/fetchLast", controller.getLast); // route to fetch the last entry from the database
router.get("/fetchGroup", controller.getGroup); // route to fetch a group of entries from the database
router.get("/fetchBy/:id", controller.getBy); // route to fetch a group of entries from the database with a parameter

module.exports = router;
