/**
 * This is the handles of the content of the routes
 */

const api = require("../services/API");
const auth = require("../services/password_handler");

// fetch all entries from the database
const getAll = (req, res) => {
  let pwdHandler = new auth(req.query.password);

  if (pwdHandler.comparePasswords()) {
    api
      .dataFetchAll()
      .then((results) => {
        res.send({ status: "OK", data: results });
      })
      .catch((err) => {
        res.send({ status: "ERROR", data: err });
      });
  } else {
    res.send({ status: "ERROR", data: "Wrong password" });
  }
};

// fetch all entries from the database without purging the database
const getAllKeep = (req, res) => {
  let pwdHandler = new auth(req.query.password);

  if (pwdHandler.comparePasswords()) {
    api
      .dataFetchAllKeep()
      .then((results) => {
        res.send({ status: "OK", data: results });
      })
      .catch((err) => {
        res.send({ status: "ERROR", data: err });
      });
  } else {
    res.send({ status: "ERROR", data: "Wrong password" });
  }
};

// fetch the last entry from the database
const getLast = (req, res) => {
  let pwdHandler = new auth(req.query.password);

  if (pwdHandler.comparePasswords()) {
    api
      .dataFetchLast()
      .then((results) => {
        res.send({ status: "OK", data: results });
      })
      .catch((err) => {
        res.send({ status: "ERROR", data: err });
      });
  } else {
    res.send({ status: "ERROR", data: "Wrong password" });
  }
};

// fetch a group of entries from the database
const getGroup = (req, res) => {
  let pwdHandler = new auth(req.query.password);

  if (pwdHandler.comparePasswords()) {
    api
      .dataFetchGroup()
      .then((results) => {
        res.send({ status: "OK", data: results });
      })
      .catch((err) => {
        res.send({ status: "ERROR", data: err });
      });
  } else {
    res.send({ status: "ERROR", data: "Wrong password" });
  }
};

// fetch a group of entries from the database with a parameter
const getBy = (req, res) => {
  let pwdHandler = new auth(req.query.password);

  if (pwdHandler.comparePasswords()) {
    api
      .dataFetchBy(req.params.id)
      .then((results) => {
        res.send({ status: "OK", data: results });
      })
      .catch((err) => {
        res.send({ status: "ERROR", data: err });
      });
  } else {
    res.send({ status: "ERROR", data: "Wrong password" });
  }
};

module.exports = {
  getAllKeep,
  getAll,
  getLast,
  getGroup,
  getBy,
};
