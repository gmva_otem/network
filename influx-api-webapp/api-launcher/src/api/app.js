/**
 * This is the main entry point of the API
 */

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const router = require("./routes/routes");

const app = express();
const PORT = process.env.PORT || 3000;

// using cors to enable cross-origin resource sharing
app.use(cors());

// using body-parser to parse JSON bodies into JS objects
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// adding a router
app.use("/", router);

// initializing the server
app.listen(PORT, () => {
  console.log("API is running");
});
