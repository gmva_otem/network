/**
 * This is the handler of the database
 */

const { InfluxDB, Point } = require("@influxdata/influxdb-client");
const host = require("./config").influx_host;
const token = require("./config").influx_token;
const org = require("./config").influx_org;
const db = require("./config").influx_db;

class InfluxHandler {
  constructor() {
    this.host = host;
    this.token = token;
    this.org = org;
    this.db = db;

    // establiching a connection with the database
    this.influxClient = new InfluxDB({
      url: this.host,
      token: this.token,
    });

    // creating a write API
    this.writeApi = this.influxClient.getWriteApi(this.org, this.db);
  }

  // inserts a new row of data in the database
  processMessage(message) {
    let p = new Point("data")
      .stringField("idSensor", message.idSensor)
      .stringField("idFloater", message.idFloater)
      .stringField("latitude", message.lat)
      .stringField("longitude", message.long)
      .stringField("measurementDate", message.date)
      .stringField("measurementTime", message.time)
      .stringField("measurementTemp", message.temp);

    this.writeApi.writePoint(p);
    console.log("insert success");
    this.writeApi.flush();
  }
}

module.exports = InfluxHandler;
