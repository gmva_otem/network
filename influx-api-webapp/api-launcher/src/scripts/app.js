/**
 * This is the main entry point for the application
 */

const mqttHandler = require("./mqtt_handler");

// creating an instance of mqttHandler
const mqttClient = new mqttHandler();
mqttClient.connect(); // connecting to the broker
