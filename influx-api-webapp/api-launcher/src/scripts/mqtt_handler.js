/**
 * This is the handler of the broker
 */

const mqtt = require("mqtt");
const host = require("./config").mqtt_host;
const username = require("./config").mqtt_username;
const password = require("./config").mqtt_password;
const influxHandler = require("./influx_handler");

class MqttHandler {
  constructor() {
    this.host = host;
    this.username = username;
    this.password = password;

    this.influxClient = new influxHandler();
  }

  connect() {
    // connecting using the user's credentials
    this.mqttClient = mqtt.connect(this.host, {
      username: this.username,
      password: this.password,
      clientId: "mqttjs_" + Math.random().toString(16).slice(3),
      port: 1883,
      keepAlive: 60000,
      encoding: "utf8",
    });

    // to execute when the client is connected
    this.mqttClient.on("connect", () => {
      console.log("client connected");

      // subscribes to all topics
      this.mqttClient.subscribe("#");
    });

    // to execute when a message is received
    this.mqttClient.on("message", (topic, message) => {
      let msg = JSON.parse(message);

      let json = {
        idSensor: msg.end_device_ids.dev_eui,
        idFloater: msg.uplink_message.rx_metadata.gateway_ids.gateway_id,
        lat: msg.uplink_message.rx_metadata.gateway_ids.latitude,
        long: msg.uplink_message.rx_metadata.gateway_ids.longitude,
        date: msg.uplink_message.decoded_payload.bytes.date,
        time: msg.uplink_message.decoded_payload.bytes.time,
        temp: msg.uplink_message.decoded_payload.bytes.temp,
      };

      setTimeout(() => {
        this.influxClient.processMessage(json);
      }, 250);
    });

    // to execute when an error occurs
    this.mqttClient.on("error", (err) => console.log("error: " + err));

    // to execute when the client is disconnected
    this.mqttClient.on("close", () => console.log("client disconnected"));
  }
}

module.exports = MqttHandler;
