// Importing the required modules
const WebSocketServer = require('ws');
const fs = require('fs');
const exec = require('child_process').exec;

log_out=""
var EUI = ""

// Creating a new websocket server
const wss = new WebSocketServer.Server({ port: 4000});

//Send datas to the client 
function sendDatas(ws){
    getLastLogs((data) =>{
        if(data != log_out){
            ws.send(JSON.stringify({log_out:data,titre:"out"}))
        }

        log_out=data;
    })     
}

//Read files
function getLastLogs(callback){
    exec("tail --lines=2 /opt/sx1302_hal/packet_forwarder/lora.out | head --lines=1",(error, stdOut, stdErr) =>{
        if(!error){
            callback(stdOut)
        }
    });
}

function getLogs(callback){
    fs.readFile('/opt/sx1302_hal/packet_forwarder/lora.out', 'ascii',(err,stdOut) =>{
        if(!err){
            tmp = stdOut.toString()
            tmp = tmp.split('\n').slice(0,tmp.length - 2)

            log_out = tmp[tmp.lentgh - 2]

            callback(tmp.join('\n'))
        }else{
            console.error(err)
        }
    });    
}


//Récupération du concentrator EUI
function getEUI(callback){
    exec('cd /opt/sx1302_hal/util_chip_id; ./chip_id |grep EUI | cut -d "x" -f2',(error, stdOut, stdErr) =>{
        if(stdOut == ""){
            getEUI(callback)
        }else{
            callback(stdOut)
        }
    })
}

function getConf(callback){
    fs.readFile('/opt/sx1302_hal/packet_forwarder/global_conf.json', 'ascii',(error, stdOut) =>{
        callback(error,stdOut.toString());     
    });
}

function setConf(json, callback){
    fs.writeFile("/opt/sx1302_hal/packet_forwarder/global_conf.json", JSON.stringify(json,null,4), (err) => {
        callback(err)
    })
}



//Set a delay and call sendDatas every n milisecond
const delay = ms => new Promise(res => setTimeout(res, ms));
async function startSending (ws){
    while (true){
        await delay(500);
        sendDatas(ws);
    }
}

//When a client connects set up listeners and startSending
wss.on("connection", ws => {

    var log_out=""

    console.log(new Date() + ' Peer ' + ws._socket.remoteAddress + ' connect')

    if(EUI == ""){
        getEUI((eui) =>{
            EUI=eui
            ws.send(JSON.stringify({EUI:EUI,titre:"EUI"}))
        })
    }else{
        ws.send(JSON.stringify({EUI:EUI,titre:"EUI"}))
    }

    getLogs((logs) =>{
        ws.send(JSON.stringify({logs:logs,titre:"logs"}))
        startSending(ws)
    })
    
    getConf((err,conf) =>{
        ws.send(JSON.stringify({global_conf:conf,titre:"Conf"}))
    })

    ws.addEventListener("message", function (event) {
        var data = event.data
        if(data == "reboot gateway"){
            exec('reboot now', function (error, stdOut, stdErr) {})
        }else if(data == "update interface"){
            exec('bash /var/www/gateway_interface/update.sh; nohup nodejs /var/www/gateway_interface/websocket-server/main.js 1> /var/www/gateway_interface/websocket-server/main.log 2> /var/www/gateway_interface/websocket-server/main.err &', function (error, stdOut, stdErr) {
                process.exit()
            })
        }else if(data == "restart service"){
            exec('killall /opt/sx1302_hal/packet_forwarder/lora_pkt_fwd; cd /opt/sx1302_hal/packet_forwarder; nohup ./lora_pkt_fwd 1> /opt/sx1302_hal/packet_forwarder/lora.out &', function (error, stdOut, stdErr) {})
        }else{
            data = JSON.parse(data)
            if(data.global_conf != undefined){
                setConf(data.global_conf,(error) =>{
                    if(error){
                        console.error(new Date() + ' Peer ' + ws._socket.remoteAddress + ' tried to change global_conf but got an error : ' + error)
                        ws.send(JSON.stringify({global_conf_response:{err : error}}))
                    }else{
                        console.log(new Date() + ' Peer ' + ws._socket.remoteAddress + ' as changed global_conf')
                        exec('killall /opt/sx1302_hal/packet_forwarder/lora_pkt_fwd; cd /opt/sx1302_hal/packet_forwarder; nohup ./lora_pkt_fwd 1> /opt/sx1302_hal/packet_forwarder/lora.out &', function (error, stdOut, stdErr) {
                            ws.send(JSON.stringify({global_conf_response:{err : error}}))
                        })
                    }
                })
            }
        }
    })

    ws.on("close", () => {
        console.log(new Date() + ' Peer ' + ws._socket.remoteAddress + ' disconnect')
    });

    ws.on("error", () => {
        console.erroror(new Date() + ' Peer ' + ws._socket.remoteAddress + ' makes error')
    })
});