var connected = false
var updateInterface = false

function connectToWebSocketServer(callback){
    connected = false
    ws = new WebSocket("ws://" + location.host + ":4000");
    ws.onerror = function (error) {
        connectToWebSocketServer(callback) 
    };
    callback(ws)
}

function sendGlobalConf(){
    try{
        console.log(document.getElementById("global_conf").value)
        ws.send(JSON.stringify({global_conf : JSON.stringify(JSON.parse(document.getElementById("global_conf").value))}))
    }catch(error){
        alert("Fichier de configuration mal formé rechargez la page pour récupérer le fichier sur la passerelle : " + error)
    }
    
}
  

function update_interface(){
    ws.send("update interface")
    ws.close()
    document.getElementById("status").innerText = "Status : Disconnected"
    connected = false
    updateInterface = true
    setTimeout(()=>{app()},"5000")
}

function restart_service(){
    ws.send("restart_service")
}

function reboot_gateway(){
    ws.send("reboot gateway")
    ws.close()
    document.getElementById("status").innerText = "Status : Disconnected"
    connected = false
    setTimeout(()=>{app()},"10000") 
}

function app(){
    connectToWebSocketServer((ws)=>{  

        ws.addEventListener('message', function (event){
            data = JSON.parse(event.data)
            if(!connected){
                document.getElementById("status").innerText = "Status : Connected"
                connected = true
            }
            if(updateInterface){
                location.reload();
            }
            if(data.log_out != undefined){
                element = document.getElementById("LGLogs")

                element.innerText = element.innerText + "\n" + data.log_out

            }else if(data.log_err != undefined){
                element = document.getElementById("LELogs")

                element.innerText = element.innerText + "\n" + data.log_err
                
            }else if(data.global_conf != undefined){
                document.getElementById("global_conf").innerText = JSON.stringify(JSON.parse(data.global_conf),null,4)
            }else if(data.EUI != undefined){
                document.getElementById("EUI").innerText = "EUI : " + data.EUI.toUpperCase()
            }else if(data.logs != undefined){
                document.getElementById("LGLogs").innerText = data.logs.toString()
            }else if(global_conf_response != undefined){
                if(global_conf_response.err){
                    alert("Got an error while saving or lauching the service : " + global_conf_response.err)
                }else{
                    alert("Changes had been deployed successfully")
                }
            }
        });
    })
}

app()