rm -rf /var/www/gateway_interface.old

mv /var/www/gateway_interface /var/www/gateway_interface.old

curl https://gitlab.com/gmva_otem/network/-/archive/Network1-gateway_interface/network-Network1-gateway_interface.tar.gz --output /var/www/gateway_interface.tar.gz

tar -vxzf /var/www/gateway_interface.tar.gz -C /var/www

mv /var/www/network-Network1-gateway_interface /var/www/gateway_interface

rm /var/www/gateway_interface.tar.gz
