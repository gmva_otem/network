Le projet a pour objectif de suivre et de valoriser des données de
suivi des températures d'eau de mer du Golfe du Morbihan. Ce suivi
pourra ainsi être intégré à l'Observatoire des effets du changement
climatique porté par Golfe du Morbihan Vannes Agglomération (GMVA).

Dans la partie réseau de ce projet, il s'agit de mettre en œuvre
l'infrastructure réseau de ce projet.

Pour ce faire, il faut

- choisir le matériel pour concevoir une passerelle LoRa/LoRaWAN;
- installer et configurer cette passerelle pour qu'elle puisse
  recevoir les données des capteurs et les retransmettre vers le
  réseau [TheThingsNetwork](https://www.thethingsnetwork.org/);
* rendre accessibles les données collectées à l'application Web de
  visualisation des données depuis TheThingsNetwork;
* Effectuer des tests en collaboration avec le groupe des capteurs
  afin de délimiter une distance maximale entre une passerelle 
  et des capteurs;
* Effectuer des études de terrain afin de positionner la/les
  passerelles.
