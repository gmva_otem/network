# Créer une image


### Introduction:
Une des manières les plus simples de copier une support de stockage est de faire une image de support. C'est à dire un clône parfait. Bien que la création de l'image soit relativement simple, le fichier .img obtenu peut être d'une taille assez conséquente, puisque lors du processus de clonage, tous le support sera copié, incluant même l'espace disponible. (Une image d'une carte SD de 32Go, fera donc 32Go même si elle ne contient qu'à peine 1Go de données.) C'est pour ça que le processus de réduction (Shrink en anglais) est une deuxième étape essentielle. 

Cette documentation traitera du processus utilisé pour créer et réduire une image sous Linux, mais il est évidemment possible de le faire sous windows, avec pour la plupart des logiciels proposant une alternative aux commandes Linux qui vont suivre.


### Etape 1: Création d'une image

Pour créer une image sous Linux, nous allons utiliser l'utilitaite **dd**.


La commande à taper est la suivante:
>  sudo dd bs=4M if=/dev/sda of=/home/utilisateur/EmplacementSouhaitéDeL'image/nomDeL'image.img status=progress

- bs est un paramètre indiquant la taille des blocs
- if corresponds à l'input file, la source que vous voulez clôner. Si c'est une carte sd, il y a de grande chance qu'elle soit répertoriée au chemin /dev/sda
- of corresponds à l'output file, le fichier qui sera la copie conforme de la source. Le fichier doit avoir l'extension .img mais peut avoir n'importe quel nom et n'importe quel chemin
- status=progress permet d'avoir un visuel sur l'avancement du processus

### Etape 2: Réduction de l'image
Pour la réduction de l'image, la majeure partie du travail sera effectuée avec l'utilitaire GParted.

> /!\\ Si GParted n'est pas installé:
> *sudo apt install gparted*

Le "problème" est que GParted ne fonctionne qu'avec des périphériques montés, et non avec des fichiers .img comme celui créé précédemment.

##### Monter le fichier .img
Nous allons donc dans un premier temps devoir monté notre fichier image pour ensuite pouvoir travailler dessus. Pour ce faire, la première chose à faire est d'activer la fonctionnalité "loopback" de Linux, si elle n'est pas déjà activée:

> *sudo modprobe loop*

Maintenant que la fonctionnalité loopback est activée, nous pouvons venir demander un espace libre avec la commande:

> *sudo losetup -f*

Cette commande devrait vous retourner un emplacement tel que: **/dev/loopX
##### Notez bien ce chemin, il sera important.

Maintenant que nous avons notre emplacement libre, nous allons dans un premier temps  charger notre fichier .img dans cet emplacement, avant de venir rendre accessibles les différentes partitions contenues dans le fichier .img: 

> *sudo losetup /dev/loopX leFichier.img*
>
> *sudo partprobe /dev/loopX*

##### Utilisation de GParted
Notre .img étant maintenant monté, l'utilitaire devrait pouvoir y accéder. Pour le lancer, si GParted est bien installé, il suffit d'entrer la commande suivante:

> *sudo gparted /dev/loopX*

Dans la fenêtre qui va apparaître, vous devriez pouvoir voir les différentes partitions de votre image. Notez la partie volumineuse qui vous intéresse, et toute la place non alloué qui devrait être visible. Pour la réduire, il faudra dans un premier temps utiliser l'option **"Redimensionner"** de GParted, sur la partition qui vous intéresse. Dans la nouvelle fenêtre, vous pourrez utiliser le curseur pour réduire la taille allouée à votre partition. Le mieux est évidemment de **bouger le curseur le plus à gauche possible** mais il est possible que **GParted refuse** car il peut avoir **besoin d'un certain volume d'espace libre**. Trouvez le meilleur **compromis**, puis **validez**.

Maintenant que votre image à été redimensionnée, nous pouvons fermer GParted et utiliser la commande suivante pour démonter notre image:

> *sudo losetup -d /dev/loopX*

##### Réduction de l'image
Bien que l'espace soit réorganisé à l'intérieur de votre fichier, sa taille globale n'est pas encore modifiée. La dernière étape, est donc la réduction de votre .img pour vous débarasser de toute la partie inutile.

Pour obtenir les informations qui nous seront nécessaire pour cette dernière étape, entrez la commande suivante:

> *sudo fdisk -l votreImage.img*

Beaucoup d'informations devraient apparaitre, mais en ce qui nous concerne, seules les valeurs de la taille des **Units** (généralement 512), du **start** et du **sectors**. Pour réduire l'image, nous allons utiliser la commande *truncate* qui viendra couper notre fichier .img à partir d'un certain nombre de blocs. Pour connaître cette valeur, il suffit de faire le calcul suivant: 

> (**start** du /dev/loopXp1  + **sectors** de chaque /dev/loopXpY) * **Units**

Une fois cet **emplacement** obtenu, il ne reste qu'une dernière commande:

> *sudo truncate --size=**EmplacementCalculé** votreImage.img*

Une fois cette dernière commande effectuée, vous pouvez aller dans les propriétés de votre fichier .img, et devriez pouvoir constater que sa taille devrait être passée de 32Go (exemple de l'introduction) à une taille bien plus acceptable.
